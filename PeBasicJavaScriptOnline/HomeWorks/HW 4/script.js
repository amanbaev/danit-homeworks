
const getMathOperation = () => {
	let userChoice;
	
	do	 {userChoice = prompt('Enter math operation')}

	  while(userChoice !== '+' && userChoice !== '-' && userChoice !== "/" && userChoice !=="*")
    
	  return userChoice;
}
const getNumber = (message) => {
	let userNumber;
	
	do {userNumber = prompt(message, userNumber)}	
	while(isNaN(+userNumber) || userNumber === '')
	return +userNumber;
}

function sum() {
    let firstNumber = getNumber('Enter first Number')
    let secondNumber = getNumber('Enter second Number')
    return firstNumber + secondNumber
}

function sub() {
    let firstNumber = getNumber('Enter first Number')
    let secondNumber = getNumber('Enter second Number')
    return firstNumber - secondNumber
}

function mult() {
    let firstNumber = getNumber('Enter first Number')
    let secondNumber = getNumber('Enter second Number')
    return firstNumber * secondNumber
}

function division() {
    let firstNumber = getNumber('Enter first Number')
    let secondNumber = getNumber('Enter second Number')
    return firstNumber / secondNumber
}

const calculateArea = () => {
	const userChoice = getMathOperation();

	switch(userChoice) {
		case "+":
			console.log(sum());
			break;
		case "-":
			console.log(sub());
			break;
		case "*":
			console.log(mult());
			break;
		case "/":
			console.log(division());
			break
	}
}
calculateArea(+)