// 1. при фокусе у поля должна быть зеленая рамка
// 2. после пропажи фокуса должен появится span с текстом
//Текущая цена: ${значение из поля ввода} и кнопка Х
// 3. при нажатия на кнопку должна удаляться и кнопка и спан и обнулять поле
// 4. если число в поле ввода меньше нуля, создавать текст Please enter correct price

let input = document.querySelector('#inputPrice') 
let span = document.getElementById("spanPrice");
let button = document.getElementById("buttonCancel");
let incorectValue = document.createElement("span");
button.addEventListener("click", () => {
  span.classList.add("hidden");
  button.classList.add("hidden");
  input.value = "";
});

input.addEventListener("focus", () => {
  input.style.border = "3px solid green";
  input.style.color = "grey";
  input.style.backgroundColor = "bisque";
  incorectValue.remove()
  // or
  // input.classList.add('inputBorederColor')
});



input.addEventListener("blur", () => {
  input.style.border = "";
  input.style.color = "";
  input.style.backgroundColor = "";
  if (input.value < 0 || input.value == '') {
  
  incorectValue.innerText = "Please enter correct price";
  input.after(incorectValue);  
  
  } else {
    span.classList.remove("hidden");
    button.classList.remove("hidden");
    span.innerText = `Текущая цена: ${input.value}`;
  }
});
